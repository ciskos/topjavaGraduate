Graduation project.
===================

Authentication data
===================
Login: Admin  
Password: admin

Login: User  
Password: user

Technical Assignment
====================
Design and implement a REST API using Hibernate/Spring/SpringMVC (or Spring-Boot) without frontend.

The task is:

Build a voting system for deciding where to have lunch.

2 types of users: admin and regular users
Admin can input a restaurant and it's lunch menu of the day (2-5 items usually, just a dish name and price)
Menu changes each day (admins do the updates)
Users can vote on which restaurant they want to have lunch at
Only one vote counted per user
If user votes again the same day:
If it is before 11:00 we asume that he changed his mind.
If it is after 11:00 then it is too late, vote can't be changed
Each restaurant provides new menu each day.

As a result, provide a link to github repository.

It should contain the code and README.md with API documentation and curl commands to get data for voting and vote.


Схема БД
========
![db_structure](https://github.com/ZZ-Kot/topjavaGraduate/blob/master/draftData/DBscheme.png)

#CURL
=====

Lunch
=====
getAll -  
curl -v http://localhost:8080/rest/lunch/

[{"id":1,"dish":"dish1","price":10.0,"restaurant":{"id":1,"name":"Restaurant1"}}  
,{"id":2,"dish":"dish2","price":20.0,"restaurant":{"id":2,"name":"Restaurant2"}}  
,{"id":3,"dish":"dish3","price":30.0,"restaurant":{"id":1,"name":"Restaurant1"}}  
,{"id":4,"dish":"dish4","price":40.0,"restaurant":{"id":2,"name":"Restaurant2"}}  
,{"id":5,"dish":"dish5","price":50.0,"restaurant":{"id":3,"name":"Restaurant3"}}  
,{"id":6,"dish":"dish6","price":60.0,"restaurant":{"id":3,"name":"Restaurant3"}}  
,{"id":7,"dish":"dish7","price":70.0,"restaurant":{"id":4,"name":"Restaurant4"}}  
,{"id":8,"dish":"dish8","price":80.0,"restaurant":{"id":4,"name":"Restaurant4"}}]

getById -  
curl -v http://localhost:8080/rest/lunch/1

{"id":1,"dish":"dish1","price":10.0,"restaurant":{"id":1,"name":"Restaurant1"}}

getByRestaurantId -  
curl -v http://localhost:8080/rest/lunch/restaurant/1

[{"id":1,"dish":"dish1","price":10.0,"restaurant":{"id":1,"name":"Restaurant1"}}
,{"id":3,"dish":"dish3","price":30.0,"restaurant":{"id":1,"name":"Restaurant1"}}]

create -  
curl -vd 'userId=1&dish=withoutPost&price=20.00&restaurantId=1' http://localhost:8080/rest/lunch - не добавляет, не админ  
curl -vd 'userId=2&dish=withoutPost&price=20.00&restaurantId=1' http://localhost:8080/rest/lunch - добавляет, админ

возвращает статус 201

update -  
curl -X PUT -vd 'userId=1&id=1&dish=updatedDish&price=2000.00' http://localhost:8080/rest/lunch - не обновляет, не админ  
curl -X PUT -vd 'userId=2&id=1&dish=updatedDish&price=2000.00' http://localhost:8080/rest/lunch - обновляет, админ

возвращает статус 204

delete -  
curl -X DELETE -vd 'userId=1' http://localhost:8080/rest/lunch/1 - не удаляет, не админ  
curl -X DELETE -vd 'userId=2' http://localhost:8080/rest/lunch/1 - удаляет, админ

возвращает статус 204

Restaurant
==========
getAll -  
curl -v http://localhost:8080/rest/restaurant/

[{"id":1,"name":"Restaurant1"}  
,{"id":2,"name":"Restaurant2"}  
,{"id":3,"name":"Restaurant3"}  
,{"id":4,"name":"Restaurant4"}]

getById -  
curl -v http://localhost:8080/rest/restaurant/1

{"id":1,"name":"Restaurant1"}

create -  
curl -vd 'userId=1&restaurantName=createdRestaurant' http://localhost:8080/rest/restaurant - не добавляет, не админ  
curl -vd 'userId=2&restaurantName=createdRestaurant' http://localhost:8080/rest/restaurant - добавляет, админ

возвращает статус 201

update -  
curl -X PUT -vd 'userId=1&id=1&name=updatedName' http://localhost:8080/rest/restaurant - не обновляет, не админ  
curl -X PUT -vd 'userId=2&id=1&name=updatedName' http://localhost:8080/rest/restaurant - обновляет, админ

возвращает статус 204

delete -  
curl -X DELETE -vd 'userId=1' http://localhost:8080/rest/restaurant/1 - не удаляет, не админ  
curl -X DELETE -vd 'userId=2' http://localhost:8080/rest/restaurant/1 - удаляет, админ

возвращает статус 204

Roles
=====
getAll -  
curl -v http://localhost:8080/rest/role/

[{"id":1,"role":"Admin","authLevel":100}  
,{"id":2,"role":"User","authLevel":0}]

getById -  
curl -v http://localhost:8080/rest/role/1

{"id":1,"role":"Admin","authLevel":100}

getByRole -  
curl -v http://localhost:8080/rest/user/role/1

[{"id":2,"name":"User2_admin","role":{"id":1,"role":"Admin","authLevel":100}}]

create -  
curl -vd 'userId=1&roleAuthLevel=1&roleName=createdRole' http://localhost:8080/rest/role - не админ, не добавляет  
curl -vd 'userId=2&roleAuthLevel=1&roleName=createdRole' http://localhost:8080/rest/role - админ, добавляет

возвращает статус 201

update -  
curl -X PUT -vd 'userId=1&roleId=3&roleName=updatedName&roleAuthLevel=10' http://localhost:8080/rest/role - не админ, не обновляет  
curl -X PUT -vd 'userId=2&roleId=3&roleName=updatedName&roleAuthLevel=10' http://localhost:8080/rest/role - админ, обновляет

возвращает статус 204

delete -  
curl -X DELETE -vd 'userId=1' http://localhost:8080/rest/role/3 - не админ, не удаляет  
curl -X DELETE -vd 'userId=2' http://localhost:8080/rest/role/3 - админ, удаляет

возвращает статус 204

Users
=====
getAll -  
curl -v http://localhost:8080/rest/user/

[{"id":1,"name":"User1_user","role":{"id":2,"role":"User","authLevel":0}}  
,{"id":2,"name":"User2_admin","role":{"id":1,"role":"Admin","authLevel":100}}  
,{"id":3,"name":"User3_user","role":{"id":2,"role":"User","authLevel":0}}  
,{"id":4,"name":"User4_user","role":{"id":2,"role":"User","authLevel":0}}]

getById -  
curl -v http://localhost:8080/rest/user/1

{"id":1,"name":"User1_user","role":{"id":2,"role":"User","authLevel":0}}

create -  
curl -vd 'userId=1&userName=createdUser&roleId=1' http://localhost:8080/rest/user - не админ, не добавляет  
curl -vd 'userId=2&userName=createdUser&roleId=1' http://localhost:8080/rest/user - админ, добавляет

возвращает статус 201

update -  
curl -X PUT -vd 'userId=1&userToUpdate=2&userName=updatedName' http://localhost:8080/rest/user - не админ, не обновляет  
curl -X PUT -vd 'userId=2&userToUpdate=1&userName=updatedName' http://localhost:8080/rest/user - админ, обновляет

возвращает статус 204

delete -  
curl -X DELETE -vd 'userId=1' http://localhost:8080/rest/user/2 - не админ, не удаляет  
curl -X DELETE -vd 'userId=2' http://localhost:8080/rest/user/1 - админ, удаляет

возвращает статус 204

Votes
=====
getAll -  
curl -v http://localhost:8080/rest/votes/1

возвращает количество голосов по данному ресторану - 3

vote -  
curl -v 'http://localhost:8080/rest/votes/vote?userId=1&restaurantId=1'

возвращает статус 201
