package io.github.zzkot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopjavaGraduateApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopjavaGraduateApplication.class, args);
	}

}
