package io.github.zzkot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.github.zzkot.model.Lunch;
import io.github.zzkot.model.Role;
import io.github.zzkot.service.LunchService;
import io.github.zzkot.service.UserService;

@RestController
@RequestMapping(path = LunchController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class LunchController {

	static final String REST_URL = "/rest/lunch";
	
	@Autowired
	private LunchService lunchService;
	
	@Autowired
	private UserService userService;
	
    @GetMapping
    public List<Lunch> getAll() {
        return lunchService.getAll();
    }
 
    @GetMapping(path = "/{id}")
	public Lunch get(@PathVariable Integer id) {
		return lunchService.getById(id);
	}

    @GetMapping(path = "/restaurant/{id}")
	public List<Lunch> getByRestaurant(@PathVariable Integer id) {
		return lunchService.getByRestaurantId(id);
	}

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
	public void create(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "dish") String dish
			, @RequestParam(name = "price") Double price
			, @RequestParam(name = "restaurantId") Integer restaurantId
			) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			lunchService.create(dish, price, restaurantId);
		}
	}

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @PutMapping
	public void update(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "id") Integer id
			, @RequestParam(name = "dish") String dish
			, @RequestParam(name = "price") Double price
			) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			lunchService.update(id, dish, price);
		}
	}

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{id}")
	public void delete(
			@RequestParam(name = "userId") Integer userId
			, @PathVariable Integer id) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			lunchService.delete(id);
		}
	}
	
}
