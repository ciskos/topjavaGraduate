package io.github.zzkot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.github.zzkot.model.Restaurant;
import io.github.zzkot.model.Role;
import io.github.zzkot.service.RestaurantService;
import io.github.zzkot.service.UserService;

@RestController
@RequestMapping(path = RestaurantController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class RestaurantController {

	static final String REST_URL = "/rest/restaurant";
	
	@Autowired
	private RestaurantService restaurantService;
	
	@Autowired
	private UserService userService;
	
    @GetMapping
    public List<Restaurant> getAll() {
        return restaurantService.getAll();
    }
 
    @GetMapping(path = "/{id}")
	public Restaurant get(@PathVariable Integer id) {
		return restaurantService.getById(id);
	}

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
	public void create(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "restaurantName") String restaurantName) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			restaurantService.create(restaurantName);
		}
	}

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
	@PutMapping
	public void update(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "id") Integer restaurantId
			, @RequestParam(name = "name") String restaurantName) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			restaurantService.update(restaurantId, restaurantName);
		}
	}

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
	public void delete(
			@RequestParam(name = "userId") Integer userId
			, @PathVariable Integer id) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			restaurantService.delete(id);
		}
	}
    
}
