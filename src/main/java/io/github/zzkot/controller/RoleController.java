package io.github.zzkot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.github.zzkot.model.Role;
import io.github.zzkot.service.RoleService;
import io.github.zzkot.service.UserService;

@RestController
@RequestMapping(path = RoleController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleController {

	static final String REST_URL = "/rest/role";
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserService userService;
	
    @GetMapping
    public List<Role> getAll() {
        return roleService.getAll();
    }
 
    @GetMapping(path = "/{id}")
	public Role get(@PathVariable Integer id) {
		return roleService.getById(id);
	}

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
	public void create(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "roleAuthLevel", defaultValue = "0") Integer roleAuthLevel
			, @RequestParam(name = "roleName") String roleName) {
    	if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			roleService.create(roleAuthLevel, roleName);
		}
	}
    
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@PutMapping
	public void update(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "roleId") Integer roleId
			, @RequestParam(name = "roleName") String roleName
			, @RequestParam(name = "roleAuthLevel") Integer roleAuthLevel
			){
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			roleService.update(roleId, roleName, roleAuthLevel);
		}
	}

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{id}")
	public void delete(
			@RequestParam(name = "userId") Integer userId
			, @PathVariable Integer id) {
		if (userService.getAuthLevel(userId) > Role.userAdminBound) {
			roleService.delete(id);
		}
	}

}
