package io.github.zzkot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.github.zzkot.model.Role;
import io.github.zzkot.model.User;
import io.github.zzkot.service.UserService;

@RestController
@RequestMapping(path = UserController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	static final String REST_URL = "/rest/user";
	
	@Autowired
	private UserService service;
	
    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }
 
    @GetMapping(path = "/{id}")
	public User get(@PathVariable Integer id) {
		return service.getById(id);
	}

    @GetMapping(path = "/role/{roleId}")
    public List<User> getByRole(@PathVariable Integer roleId) {
    	return service.getByRoleId(roleId);
    }
    
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
	public void create(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "userName") String userName
			, @RequestParam(name = "roleId") Integer roleId
			) {
		if (service.getAuthLevel(userId) > Role.userAdminBound) {
			service.create(userName, roleId);
		}
	}

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
	@PutMapping
	public void update(
			@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "userToUpdate") Integer userToUpdate
			, @RequestParam(name = "userName") String userName
			) {
    	if (service.getAuthLevel(userId) > Role.userAdminBound) {
			service.update(userToUpdate, userName);
		}
	}

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{userToDelete}")
	public void delete(@RequestParam(name = "userId") Integer userId
					, @PathVariable Integer userToDelete) {
    	if (service.getAuthLevel(userId) > Role.userAdminBound) {
			service.delete(userToDelete);
		}
	}

}
