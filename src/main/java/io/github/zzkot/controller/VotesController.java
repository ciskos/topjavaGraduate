package io.github.zzkot.controller;

import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.github.zzkot.service.VotesService;

@RestController
@RequestMapping(path = VotesController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class VotesController {

	static final String REST_URL = "/rest/votes";
	
	private static final int voteBeforeMinutes = 00;
	private static final int voteBeforeHours = 11;
	
	@Autowired
	private VotesService service;
 
    @GetMapping(path = "/{id}")
	public Integer getCount(@PathVariable(name = "id") Integer restaurantId) {
		return service.countByRestaurantId(restaurantId);
	}

    /*
     * TODO
     * 		Условия голосования - 
     *		-Only one vote counted per user
     *		-If user votes again the same day:
     *		-If it is before 11:00 we asume that he changed his mind.
     *		-If it is after 11:00 then it is too late, vote can't be changed
     * */
    @ResponseStatus(code = HttpStatus.CREATED)
    @GetMapping(path = "/vote")
	public void vote(@RequestParam(name = "userId") Integer userId
			, @RequestParam(name = "restaurantId") Integer restaurantId) {
    	
    	LocalTime limit = LocalTime.of(voteBeforeHours, voteBeforeMinutes);
    			
    	if (LocalTime.now().isBefore(limit)) {
			if (service.getByUserIdAndRestaurantIdIsEmpty(userId, restaurantId)) {
				service.increaseVote(userId, restaurantId);
			} else {
				service.decreaseVote(userId, restaurantId);
			} 
		}

	}
    
}
