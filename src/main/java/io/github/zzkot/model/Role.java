package io.github.zzkot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "roles")
public class Role {
	
	public static final Integer userAdminBound = 10;

	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
    @Column(name = "role")
    private String role;

    @Range(min = 0, max = 100, message = "range 0-100")
    @Column(name = "authLevel", columnDefinition = "integer default 0")
    private Integer authLevel;
    
	public Role() {
	}

	public Role(Integer id, String role, @Range(min = 0, max = 100, message = "range 0-100") Integer authLevel) {
		this.id = id;
		this.role = role;
		this.authLevel = authLevel;
	}

	public Integer getId() {
		return id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getAuthLevel() {
		return authLevel;
	}

	public void setAuthLevel(Integer authLevel) {
		this.authLevel = authLevel;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", role=" + role + ", authLevel=" + authLevel + "]";
	}

}
