package io.github.zzkot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.github.zzkot.model.Lunch;

@Repository
public interface LunchRepository extends JpaRepository<Lunch, Integer> {
	
	List<Lunch> findByRestaurantId(Integer restaurantId);

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO lunch(dish, price, restaurant_id) VALUES(:dish, :price, :restaurantId)", nativeQuery = true)
	void createLunch(@Param("dish") String dish, @Param("price") Double price, @Param("restaurantId") Integer restaurantId); 

	@Modifying
	@Transactional
	@Query(value = "UPDATE lunch SET dish=:dish, price=:price WHERE id=:id", nativeQuery = true)
	void updateLunch(@Param("id") Integer id, @Param("dish") String dish, @Param("price") Double price); 
	
}
