package io.github.zzkot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.github.zzkot.model.Restaurant;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO restaurant(name) VALUES(:name)", nativeQuery = true)
	void createRestaurant(@Param("name") String name);

	@Modifying
	@Transactional
	@Query(value = "UPDATE restaurant SET name=:name WHERE id=:id", nativeQuery = true)
	void updateRestaurant(@Param("id") Integer id, @Param("name") String name);

}
