package io.github.zzkot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.github.zzkot.model.Role;

@Repository
public interface RolesRepository extends JpaRepository<Role, Integer> {

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO roles(auth_level, role) VALUES(:authLevel, :name)", nativeQuery = true)
	void createRole(@Param("authLevel") Integer authLevel, @Param("name") String name); 

	@Modifying
	@Transactional
	@Query(value = "UPDATE roles SET auth_level=:authLevel, role=:name WHERE id=:id", nativeQuery = true)
	void updateRole(@Param("id") Integer id, @Param("name") String name, @Param("authLevel") Integer authLevel); 

}
