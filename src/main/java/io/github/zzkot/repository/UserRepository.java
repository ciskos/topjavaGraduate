package io.github.zzkot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.github.zzkot.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	List<User> findByRoleId(Integer roleId);

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO users(name, role_id) VALUES(:name, :roleId)", nativeQuery = true)
	void createUser(@Param("name") String name, @Param("roleId") Integer roleId); 

	@Modifying
	@Transactional
	@Query(value = "UPDATE users SET name=:name WHERE id=:id", nativeQuery = true)
	void updateUser(@Param("id") Integer id, @Param("name") String name); 

}
