package io.github.zzkot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.github.zzkot.model.Vote;

@Repository
public interface VotesRepository extends JpaRepository<Vote, Integer> {
	
	List<Vote> findAllByUserIdAndRestaurantId(Integer userId, Integer restaurantId);
	
	List<Vote> findAllByRestaurantId(Integer restaurantId);
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO votes(user_id, restaurant_id) VALUES(:userId, :restaurantId)", nativeQuery = true)
	void increaseVote(@Param("userId") Integer userId, @Param("restaurantId") Integer restaurantId); 

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM votes WHERE user_id=:userId AND restaurant_id=:restaurantId", nativeQuery = true)
	void decreaseVote(@Param("userId") Integer userId, @Param("restaurantId") Integer restaurantId); 

}
