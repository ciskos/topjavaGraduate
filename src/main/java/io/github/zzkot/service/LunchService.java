package io.github.zzkot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.zzkot.model.Lunch;
import io.github.zzkot.repository.LunchRepository;

@Service
public class LunchService {
	
	@Autowired
	private LunchRepository repository;
	
    public List<Lunch> getAll() {
    	return repository.findAll();
    }

    public Lunch getById(Integer id) {
    	Optional<Lunch> lunch = repository.findById(id);
    	
    	if (lunch.isPresent()) return lunch.get();
    	
    	return new Lunch();
    }
    
    public List<Lunch> getByRestaurantId(Integer restaurantId) {
    	return repository.findByRestaurantId(restaurantId);
    }
    
    public void create(String dish, Double price, Integer restaurantId) {
    	repository.createLunch(dish, price, restaurantId);
    }

    public void update(Integer lunchId, String dish, Double price) {
    	repository.updateLunch(lunchId, dish, price);
    }
    
    public void delete(Integer id) {
    	repository.deleteById(id);
    }
    
}
