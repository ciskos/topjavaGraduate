package io.github.zzkot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.zzkot.model.Restaurant;
import io.github.zzkot.repository.RestaurantRepository;

@Service
public class RestaurantService {
	
	@Autowired
	private RestaurantRepository repository;
	
    public List<Restaurant> getAll() {
    	return repository.findAll();
    }

    public Restaurant getById(Integer id) {
    	Optional<Restaurant> restaurant = repository.findById(id);
    	
    	if (restaurant.isPresent()) return restaurant.get();
    	
    	return new Restaurant();
    }
    
    public void create(String name) {
    	repository.createRestaurant(name);
    }

    public void update(Integer id, String name) {
    	repository.updateRestaurant(id, name);
    }
    
    public void delete(Integer id) {
    	repository.deleteById(id);
    }

}
