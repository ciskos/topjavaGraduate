package io.github.zzkot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.zzkot.model.Role;
import io.github.zzkot.repository.RolesRepository;

@Service
public class RoleService {
	
	@Autowired
	private RolesRepository repository;
	
    public List<Role> getAll() {
    	return repository.findAll();
    }

    public Role getById(Integer id) {
    	Optional<Role> role = repository.findById(id);
    	
    	if (role.isPresent()) return role.get();
    	
    	return new Role();
    }
    
    public void create(Integer authLevel, String name) {
    	repository.createRole(authLevel, name);
    }

    public void update(Integer id, String name, Integer authLevel) {
    	repository.updateRole(id, name, authLevel);
    }
    
    public void delete(Integer id) {
    	repository.deleteById(id);
    }
    
}
