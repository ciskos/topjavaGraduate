package io.github.zzkot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.zzkot.model.User;
import io.github.zzkot.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
    public List<User> getAll() {
    	return repository.findAll();
    }

    public User getById(Integer id) {
    	Optional<User> user = repository.findById(id);

    	if (user.isPresent()) return user.get();
    	
    	return new User();
    }
    
    public List<User> getByRoleId(Integer roleId) {
    	return repository.findByRoleId(roleId);
    }
    
    public Integer getAuthLevel(Integer id) {
    	Optional<User> user = repository.findById(id);

    	if (user.isPresent()) return user.get().getRole().getAuthLevel();
    	
    	return -1;
    }
    
    public void create(String name, Integer roleId) {
    	repository.createUser(name, roleId);
    }

    public void update(Integer id, String name) {
    	repository.updateUser(id, name);
    }
    
    public void delete(Integer id) {
    	repository.deleteById(id);
    }

}
