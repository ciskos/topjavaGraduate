package io.github.zzkot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.zzkot.repository.VotesRepository;

@Service
public class VotesService {
	
	@Autowired
	private VotesRepository repository;
	
    public Integer countByRestaurantId(Integer restaurantId) {
    	return repository.findAllByRestaurantId(restaurantId).size();
    }

    public boolean getByUserIdAndRestaurantIdIsEmpty(Integer userId, Integer restaurantId) {
    	return repository.findAllByUserIdAndRestaurantId(userId, restaurantId).isEmpty();
    }

    public void increaseVote(Integer userId, Integer restaurantId) {
//    	if (getByUserIdAndRestaurantId(userId, restaurantId)) {
			repository.increaseVote(userId, restaurantId);
//		}
    }

    public void decreaseVote(Integer userId, Integer restaurantId) {
//    	if (!getByUserIdAndRestaurantId(userId, restaurantId)) {
    		repository.decreaseVote(userId, restaurantId);
//    	}
    }

}
