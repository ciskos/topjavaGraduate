INSERT INTO roles(auth_level, role) VALUES(100, 'Admin');
INSERT INTO roles(role) VALUES('User');

INSERT INTO users(name, role_id) VALUES('User1_user', 2);
INSERT INTO users(name, role_id) VALUES('User2_admin', 1);
INSERT INTO users(name, role_id) VALUES('User3_user', 2);
INSERT INTO users(name, role_id) VALUES('User4_user', 2);

INSERT INTO restaurant(name) VALUES('Restaurant1');
INSERT INTO restaurant(name) VALUES('Restaurant2');
INSERT INTO restaurant(name) VALUES('Restaurant3');
INSERT INTO restaurant(name) VALUES('Restaurant4');

INSERT INTO lunch(restaurant_id, dish, price) VALUES(1, 'dish1', 10.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(2, 'dish2', 20.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(1, 'dish3', 30.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(2, 'dish4', 40.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(3, 'dish5', 50.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(3, 'dish6', 60.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(4, 'dish7', 70.0);
INSERT INTO lunch(restaurant_id, dish, price) VALUES(4, 'dish8', 80.0);

INSERT INTO votes(user_id, restaurant_id) VALUES(1, 1);
INSERT INTO votes(user_id, restaurant_id) VALUES(2, 2);
INSERT INTO votes(user_id, restaurant_id) VALUES(1, 3);
INSERT INTO votes(user_id, restaurant_id) VALUES(2, 4);
INSERT INTO votes(user_id, restaurant_id) VALUES(3, 1);
INSERT INTO votes(user_id, restaurant_id) VALUES(3, 1);
INSERT INTO votes(user_id, restaurant_id) VALUES(4, 2);
INSERT INTO votes(user_id, restaurant_id) VALUES(4, 3);
