package io.github.zzkot.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class LunchControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testGetAll() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/lunch")
		.then()
			.body(containsString("Restaurant1"), containsString("Restaurant2"));
	}

	@Test
	void testGet() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/lunch/1")
		.then()
			.body(containsString("Restaurant1"));
	}

	@Test
	void testGetByRestaurant() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/lunch/restaurant/1")
		.then()
			.body(containsString("Restaurant1"));
	}

	@Test
	void testCreate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.post("/rest/lunch?userId=2&dish=withoutPost&price=20.00&restaurantId=1")
		.then()
			.status(HttpStatus.CREATED);
	}

	@Test
	void testUpdate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.put("/rest/lunch?userId=2&id=1&dish=updatedDish&price=2000.00")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

	@Test
	void testDelete() {
		given()
			.mockMvc(mockMvc)
		.when()
			.delete("/rest/lunch/1?userId=2")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

}
