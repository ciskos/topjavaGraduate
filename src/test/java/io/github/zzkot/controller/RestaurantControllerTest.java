package io.github.zzkot.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class RestaurantControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testGetAll() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/restaurant")
		.then()
			.body(containsString("Restaurant1"), containsString("Restaurant2"));
	}

	@Test
	void testGet() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/restaurant/1")
		.then()
			.body(containsString("Restaurant1"));
	}

	@Test
	void testCreate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.post("/rest/restaurant?userId=2&restaurantName=createdRestaurant")
		.then()
			.status(HttpStatus.CREATED);
	}

	@Test
	void testUpdate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.put("/rest/restaurant?userId=2&id=1&name=updatedName")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

	@Test
	void testDelete() {
		given()
			.mockMvc(mockMvc)
		.when()
			.delete("/rest/restaurant/1?userId=2")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

}
