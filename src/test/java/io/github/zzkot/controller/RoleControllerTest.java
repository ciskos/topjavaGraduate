package io.github.zzkot.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class RoleControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testGetAll() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/role")
		.then()
			.body(containsString("Admin"), containsString("User"));
	}

	@Test
	void testGet() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/role/1")
		.then()
			.body(containsString("Admin"));
	}

	@Test
	void testCreate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.post("/rest/role?userId=2&roleAuthLevel=1&roleName=createdRole")
		.then()
			.status(HttpStatus.CREATED);
	}

	@Test
	void testUpdate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.put("/rest/role?userId=2&roleId=3&roleName=updatedName&roleAuthLevel=10")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

	@Test
	void testDelete() {
		given()
			.mockMvc(mockMvc)
		.when()
			.delete("/rest/role/1?userId=2")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

}
