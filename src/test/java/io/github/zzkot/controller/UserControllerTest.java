package io.github.zzkot.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testGetAll() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/user")
		.then()
			.body(containsString("User1_user"), containsString("User2_admin"));
	}

	@Test
	void testGet() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/user/1")
		.then()
			.body(containsString("User1_user"));
	}

	@Test
	void testGetByRole() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/user/role/1")
		.then()
			.body(containsString("User2_admin"));
	}

	@Test
	void testCreate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.post("/rest/user?userId=2&userName=createdUser&roleId=1")
		.then()
			.status(HttpStatus.CREATED);
	}

	@Test
	void testUpdate() {
		given()
			.mockMvc(mockMvc)
		.when()
			.put("/rest/user?userId=2&userToUpdate=1&userName=updatedName")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

	@Test
	void testDelete() {
		given()
			.mockMvc(mockMvc)
		.when()
			.delete("/rest/user/1?userId=2")
		.then()
			.status(HttpStatus.NO_CONTENT);
	}

}
