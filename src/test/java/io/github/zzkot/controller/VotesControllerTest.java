package io.github.zzkot.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class VotesControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testGetCount() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/votes/1")
		.then()
			.body(containsString("3"));
	}

	@Test
	void testVote() {
		given()
			.mockMvc(mockMvc)
		.when()
			.get("/rest/votes/vote?userId=1&restaurantId=1")
		.then()
			.status(HttpStatus.CREATED);
	}

}
